package zack.helixmidifunctiongenerator;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.Thread.State;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.swing.JButton;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MidiSender implements ActionListener {
	private static Logger LOG = LoggerFactory.getLogger(MidiSender.class);

	public static final int DEFAULT_MIDI_CHANNEL = 4;
	public static final int DEFAULT_MIDI_CC = 9;
	public static final char[] RUN_STRING = "|/-\\".toCharArray();

	private ShortMessage myMsg = new ShortMessage();
	private static Receiver myReceiver = null;
	private Thread senderThread;
	private boolean myIsPlaying;
	private static String myConnectionStatus = "Not connected";
	private DataPanel myDataPanel;
	private int myChannel = DEFAULT_MIDI_CHANNEL;
	private int myCC = DEFAULT_MIDI_CC;
	public static int DEFAULT_SLEEP_TIME_IN_MS = 10;

	public int getChannel() {
		return myChannel;
	}

	public void setChannel(int channel) {
		myChannel = channel;
		LOG.info("Channel: {}", myChannel);
	}

	public int getCC() {
		return myCC;
	}

	public void setCC(int cc) {
		myCC = cc;
		LOG.info("CC: {}", myCC);
	}

	public MidiSender() {
		if (myReceiver == null) {
			myReceiver = getHelixMidiTransmitterOrNull();
		} else {
			LOG.debug("Receiver was already set");
		}

		senderThread = new Thread(new Runnable() {
			private int runIndex;
			private long last = 0;

			@Override
			public void run() {
				while (true) {
					if (myReceiver != null) {
						for (int x = 0; x < myDataPanel.getNrOFvalues(); x++) {
							if (myIsPlaying) {
								int y = FunctionGenerator.getY(x);
								try {
									myMsg.setMessage(ShortMessage.CONTROL_CHANGE, myChannel - 1, myCC, y);// 3=Channel4!
									sendMidi(myMsg, myReceiver);
								} catch (InvalidMidiDataException e) {
									LOG.error("Error sending midi", e);
								}
							}
							sleep();
						}
					} else {
						sleep();
					}
				}
			}

			private void sleep() {
				try {

					long now = System.currentTimeMillis();
					if ((now - last) > 1000) { // Update title every second
						last = now;

						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								String title = myDataPanel.getFrame().getTitle();
								runIndex++;
								if (runIndex == RUN_STRING.length) {
									runIndex = 0;
								}

								char addon;

								if (myIsPlaying) {
									addon = RUN_STRING[runIndex];

								} else {
									addon = ' ';
								}

								title = title.substring(0, title.length() - 1) + addon;

								myDataPanel.getFrame().setTitle(title);
							}
						});
					}
					Thread.sleep(myDataPanel.getSleepTime());
				} catch (InterruptedException e) {
					LOG.error("Error sleeping", e);
				}
			}
		});
	}

	public void toggleMidiSending() throws InvalidMidiDataException, InterruptedException {
		synchronized (this) {
			if (myReceiver == null) {
				myReceiver = getHelixMidiTransmitterOrNull();
			}
		}

		myIsPlaying = !myIsPlaying;
		LOG.debug("loopMidi {}", myIsPlaying);
		if (senderThread.getState() == State.NEW) {
			senderThread.start();
		}
	}

	private void sendMidi(ShortMessage myMsg, Receiver r) {
		if (r != null) {
			r.send(myMsg, getSystemTimeInMicroseconds());
		}
	}

	synchronized private static Receiver getHelixMidiTransmitterOrNull() {
		try {

			for (Info device : MidiSystem.getMidiDeviceInfo()) {
				LOG.info("{} : {} : {} : {}", device.getName(), device.getVendor(), device.getVersion(),
						device.getDescription());
				if (device.getName().contains("Helix")) {
					if (device.getDescription().contains("External")) {
						MidiDevice dev = MidiSystem.getMidiDevice(device);
						if (!dev.isOpen()) {
							dev.open();
						}

						try {
							Receiver r = dev.getReceiver();
							LOG.info("Connected to {} : {} : {} : {}", device.getName(), device.getVendor(),
									device.getVersion(), device.getDescription());
							myConnectionStatus = device.getName() + " - " + device.getDescription();
							return r;
						} catch (Exception e) {
							LOG.error("Error getting midi receiver", e);
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Error getting midi device", e);
		}
		return null;
	}

	synchronized public void setHelixMidiTransmitter(Info device) {
		try {

			MidiDevice dev = MidiSystem.getMidiDevice(device);
			if (!dev.isOpen()) {
				dev.open();
			}

			try {
				Receiver r = dev.getReceiver();
				LOG.info("Connected to {} : {} : {} : {}", device.getName(), device.getVendor(), device.getVersion(),
						device.getDescription());
				myConnectionStatus = device.getName() + " - " + device.getDescription();
				myReceiver = r;
			} catch (Exception e) {
				LOG.error("Error getting midi receiver", e);
			}

		} catch (

		Exception e) {
			LOG.error("Error getting midi device", e);
		}
	}

	static long getSystemTimeInMicroseconds() {
		return System.currentTimeMillis() * 1000;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		LOG.debug("actionPerformed {}", e);
		try {
			toggleMidiSending();
		} catch (InvalidMidiDataException | InterruptedException e1) {
			LOG.error("Error toggling midi", e1);
		}

		if (e != null) {
			if (e.getSource() instanceof JButton) {
				JButton jb = ((JButton) e.getSource());
				if (myIsPlaying) {
					jb.setBackground(Color.GREEN);
				} else {
					jb.setBackground(Color.GRAY);
				}
				jb.setOpaque(true);
				jb.setBorderPainted(false);
			}
		}
		if (myDataPanel != null) {
			myDataPanel.refreshData();
		}
	}

	public void exit() {
		System.exit(0);
	}

	public String getConnectionStatus() {
		return myConnectionStatus;
	}

	public String getSenderStatus() {
		return myIsPlaying && (myReceiver != null) ? "Sending midi" : "Sending off";
	}

	public void setDataPanel(DataPanel dataPanel) {
		myDataPanel = dataPanel;
	}
}
