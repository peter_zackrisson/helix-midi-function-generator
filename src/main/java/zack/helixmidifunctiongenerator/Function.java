package zack.helixmidifunctiongenerator;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class Function {

	@JsonProperty("Name")
	private String Name;

	@JsonProperty("Function")
	private String Function;

	@JsonProperty("Values")
	private Integer Values;

	@JsonProperty("SleepTime")
	private Integer SleepTime;

	// Without this default constructor json reader will not work
	public Function() {
		super();
	}

	public Function(String Name, String Function) {
		this.Name = Name;
		this.Function = Function;
		this.Values = FunctionGenerator.NUMBER_OF_VALUES;
		this.SleepTime = MidiSender.DEFAULT_SLEEP_TIME_IN_MS;
	}

	public Function(String Name, String Function, Integer Values, Integer SleepTime) {
		this.Name = Name;
		this.Function = Function;
		this.Values = Values;
		this.SleepTime = SleepTime;
	}

	public Integer getSleepTime() {
		return SleepTime;
	}

	public void setSleepTime(Integer sleepTime) {
		SleepTime = sleepTime;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getFunction() {
		return Function;
	}

	public void setFunction(String function) {
		Function = function;
	}

	public Integer getValues() {
		return Values;
	}

	public void setValues(Integer values) {
		Values = values;
	}

	@Override
	public String toString() {
		return "Function [Name=\"" + Name + "\", Function=\"" + Function + "\", Values=" + Values + ", SleepTime="
				+ SleepTime + "]";
	}

}