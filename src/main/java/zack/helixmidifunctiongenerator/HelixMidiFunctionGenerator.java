package zack.helixmidifunctiongenerator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelixMidiFunctionGenerator {
	private static Logger LOG = LoggerFactory.getLogger(HelixMidiFunctionGenerator.class);
	public static final String DELETE_COMMAND = "Delete";
	public static final String SAVE_COMMAND = "Save         Ctrl-S";
	private static int instancesRunning = 0;
	DataPanel myDataPanel;

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException {

		LOG.info("HelixMidiFunctionGenerator started with # arguments {}", args.length);

		File f = new File(InstanceReader.INSTANCES_FILENAME);
		if (f.exists() && !f.isDirectory()) {
			for (Instance instance : InstanceReader.readFunctionsFromFile()) {
				Thread t = new Thread(() -> {
					try {
						HelixMidiFunctionGenerator h = new HelixMidiFunctionGenerator();
						instancesRunning++;
						h.runInstance(instance);
					} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
							| UnsupportedLookAndFeelException e) {
						LOG.error("Error starting instance", e);
					}
				});
				t.setName(instance.getInstanceName());

				t.run();
			}
		} else {
			HelixMidiFunctionGenerator h = new HelixMidiFunctionGenerator();
			instancesRunning++;
			h.runInstance(new Instance("", DataPanel.DEFAULT_FUNCTION_SINE, MidiSender.DEFAULT_MIDI_CHANNEL,
					MidiSender.DEFAULT_MIDI_CC));
		}
	}

	private void runInstance(Instance instance) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException {
		ReadPom r = new ReadPom();
		// Thread.currentThread().setName(instance.getInstanceName());

		String title = "Helix Midi Function Generator " + r.getVersion() + " " + instance.getInstanceName();
		LOG.info("Start instance '{}' [{}]", title, instancesRunning);
		JFrame frame = new JFrame(title + "  ");
		final MidiSender midisender = new MidiSender();
		instance.setMidiSender(midisender);
		midisender.setCC(instance.getMidiCC());

		frame.getContentPane().setLayout(new BorderLayout());
		// frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setPreferredSize(new Dimension(1400, 1000));

		String className = UIManager.getSystemLookAndFeelClassName();
		UIManager.setLookAndFeel(className);

		JPanel commandPanel = new JPanel(new FlowLayout());
		JButton openButton = new JButton("Toggle Midi   Ctrl-M");
		JButton plotButton = new JButton("Plot         Ctrl-P");
		JButton saveButton = new JButton(SAVE_COMMAND);
		JButton deleteButton = new JButton(DELETE_COMMAND);
		JButton setMidiButton = new JButton("Set Midi");
		JButton quitButton = new JButton("Quit         Ctrl-Q");

		quitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LOG.info("Exit by button {}", frame.getTitle());
				exitInstance(frame);
			}
		});

		commandPanel.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// midisender.exit();
				LOG.info("Exit by key command {}", frame.getTitle());
				exitInstance(frame);

			}
		}, KeyStroke.getKeyStroke("control Q"), JComponent.WHEN_IN_FOCUSED_WINDOW);
		commandPanel.add(openButton);
		commandPanel.add(plotButton);
		commandPanel.add(saveButton);
		commandPanel.add(deleteButton);
		commandPanel.add(setMidiButton);
		commandPanel.add(quitButton);
		frame.getContentPane().add(commandPanel, "North");

		GraphPanel graphpanel = new GraphPanel(frame, instance);

		plotButton.addActionListener(graphpanel);
		commandPanel.registerKeyboardAction(graphpanel, KeyStroke.getKeyStroke("control P"),
				JComponent.WHEN_IN_FOCUSED_WINDOW);

		openButton.addActionListener(midisender);
		commandPanel.registerKeyboardAction(midisender, KeyStroke.getKeyStroke("control M"),
				JComponent.WHEN_IN_FOCUSED_WINDOW);

		myDataPanel = graphpanel.getDataPanel(); // No need to open

		JsonWriter jsonWriter = new JsonWriter(myDataPanel);
		saveButton.addActionListener(jsonWriter);
		deleteButton.addActionListener(jsonWriter);
		commandPanel.registerKeyboardAction(jsonWriter, KeyStroke.getKeyStroke("control S"),
				JComponent.WHEN_IN_FOCUSED_WINDOW);

		setMidiButton.addActionListener(e -> createMidiDialog());

		myDataPanel.setMidiSender(midisender);
		myDataPanel.setGraphPanel(graphpanel);
		frame.setVisible(true);
		frame.pack();
		if (instance.getStart() == true) {
			midisender.actionPerformed(null);
		}
	}

	private void exitInstance(JFrame frame) {
		LOG.info("Instances: {}", instancesRunning);
		frame.setVisible(false); // you can't see me!
		frame.dispose(); // Destroy the JFrame object
		if (--instancesRunning == 0) {
			LOG.info("Last exit");
			frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
			// System.exit(0);
		}
	}

	private Object createMidiDialog() {
		try {
			MidiDialog dialog = new MidiDialog(new MidiSender(), myDataPanel);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
