package zack.helixmidifunctiongenerator;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class GraphicPanel extends JPanel {
	private static Logger LOG = LoggerFactory.getLogger(GraphicPanel.class);
	private static final float PLOT_Y_OFFSET = 50f;
	private static final int PLOT_HEIGTH = 800;
	private static final float PLOT_X_OFFSET = 50f;
	private static final int PLOT_WIDTH = 800;
	private static final long serialVersionUID = 1L;
	private boolean display_plot;
	private DataPanel dataPanel;

	@Override
	public void paintComponent(Graphics g) {
		LOG.debug("paintComponent");
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setPaint(Color.black);
		g2d.setStroke(new BasicStroke());
		g2d.setFont(new Font("Century Schoolbook", Font.PLAIN, 12));
		if (dataPanel.isInitialized() && display_plot) {
			// if (display_plot) {
			dataPanel.refreshData();
			Float xLower = 0.0f;// d.getXLower();
			Float xUpper = Float.valueOf(dataPanel.getNrOFvalues()); // d.getXUpper();
			Float xInterval = 10.0f;// d.getXInterval();
			Float yLower = 0.0f; // d.getYLower();
			Float yUpper = 130.0f; // d.getYUpper();
			Float yInterval = 10.0f;// d.getYInterval();
			Float dx = xUpper - xLower;
			Float dy = yUpper - yLower;

			g2d.setPaint(Color.gray);
			// Vertical lines
			for (Float x = PLOT_X_OFFSET; x <= PLOT_WIDTH + PLOT_X_OFFSET; x += PLOT_WIDTH * xInterval / dx)
				g2d.draw(new Line2D.Float(x, PLOT_HEIGTH + PLOT_Y_OFFSET, x, PLOT_Y_OFFSET));
			// Horizontal lines
			for (Float y = PLOT_Y_OFFSET; y <= PLOT_HEIGTH + PLOT_Y_OFFSET; y += PLOT_HEIGTH * yInterval / dy)
				g2d.draw(new Line2D.Float(PLOT_X_OFFSET - 5, y, PLOT_WIDTH + PLOT_X_OFFSET, y));

			g2d.setPaint(Color.red);

			drawGraph(g2d, xLower, yLower, dx, dy);

			drawCenteredString(g2d, dataPanel.getTitle(), 250, 25, (float) 0.);
			drawCenteredString(g2d, dataPanel.getXTitle(), 250, 475, (float) 0.);
			drawCenteredString(g2d, dataPanel.getYTitle(), 25, 250, (float) -Math.PI / 2);
			drawCenteredString(g2d, String.valueOf(xLower.intValue()), 50, 865, 0);
			drawCenteredString(g2d, String.valueOf(xUpper.intValue()), 850, 865, 0);
			drawCenteredString(g2d, String.valueOf(yLower.intValue()), 25, 847, 0);
			drawCenteredString(g2d, String.valueOf(yUpper.intValue()), 25, 50, 0);
		}
	}

	private void drawGraph(Graphics2D g2d, Float xLower, Float yLower, Float dx, Float dy) {
		Float diam = 8f;

		// String functionStr = "(Math.sin(x*2*π/100)+1)*127/2;";
		String functionStr = dataPanel.getFormula();
		int[] values = FunctionGenerator.evaluateFunction(functionStr, dataPanel.getNrOFvalues());

		int num_points = dataPanel.getNrOFvalues();
		for (int i = 0; i < num_points; i++) {
			Float ex = PLOT_WIDTH * (i - xLower) / dx + PLOT_X_OFFSET;
			ex -= diam / 2;
			Float ey = -PLOT_HEIGTH * (values[i] - yLower) / dy + PLOT_HEIGTH + PLOT_Y_OFFSET;
			ey -= diam / 2;
			g2d.fill(new Ellipse2D.Float(ex, ey, diam, diam));
		}
	}

	void setDataPanel(DataPanel new_d) {
		dataPanel = new_d;
	}

	void setDisplayPlot(boolean new_display) {
		display_plot = new_display;
	}

	private void drawCenteredString(Graphics2D g2d, String string, int x0, int y0, float angle) {
		FontRenderContext frc = g2d.getFontRenderContext();
		Rectangle2D bounds = g2d.getFont().getStringBounds(string, frc);
		LineMetrics metrics = g2d.getFont().getLineMetrics(string, frc);
		if (angle == 0) {
			g2d.drawString(string, x0 - (float) bounds.getWidth() / 2, y0 + metrics.getHeight() / 2);
		} else {
			g2d.rotate(angle, x0, y0);
			g2d.drawString(string, x0 - (float) bounds.getWidth() / 2, y0 + metrics.getHeight() / 2);
			g2d.rotate(-angle, x0, y0);
		}
	}
}
