package zack.helixmidifunctiongenerator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FunctionGenerator {
	private static Logger LOG = LoggerFactory.getLogger(FunctionGenerator.class);

	private static final int MIDI_MAX_127 = 127;
	private static final int MIDI_MIN_0 = 0;
	public static final int NUMBER_OF_VALUES = 100;

	// https://stackoverflow.com/questions/1073606/is-there-a-one-line-function-that-generates-a-triangle-wave
	static Map<String, Function> defaultFormulaMap = new HashMap<String, Function>() {
		private static final long serialVersionUID = 1L;
		{
			put("Sine", new Function("Sine", "(Math.sin(x*2*π/100)+1)*127/2;"));
			put("Square", new Function("Square", "(x % 25) < 13 ? 127 : 0;"));
			put("Sawtooth", new Function("SawTooth", "((x % 25)*2 + 40);"));
			put("Triangle", new Function("Triangle", "Math.abs((x % 25)-12)*4+50;"));
			put("Strange", new Function("Strange", "(Math.sin(x*x*0.094*3.14/100)+0.5)*127;"));
		}
	};

	private static int[] results;

	public static int[] evaluateFunction(String functionStr, int numberOfValues) {
		ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
		ScriptEngine nashorn = scriptEngineManager.getEngineByName("nashorn");
		results = new int[numberOfValues];
		Double result;
		try {
			for (int x = 0; x < numberOfValues; x++) {
				functionStr = functionStr.replaceAll("exp", "e§p");
				functionStr = functionStr.replaceAll("max", "ma§");
				functionStr = functionStr.replaceAll("expm1", "e§pm1");

				functionStr = functionStr.replaceAll("π", String.valueOf(Math.PI));
				String functionWithVal = functionStr.replaceAll("x", String.valueOf((double) x));

				functionWithVal = functionWithVal.replaceAll("§", "x");

				Object evalResult = nashorn.eval(functionWithVal);
				int intResult;
				if (evalResult == null) {
					intResult = 0;
				} else {
					result = Double.parseDouble(evalResult.toString());
					intResult = result.intValue();
				}
				results[x] = limitMidi(intResult);
			}

		} catch (ScriptException e) {
			LOG.error("Error executing script: ", e);
		}
		return results;
	}

	private static int limitMidi(int intResult) {
		if (intResult > MIDI_MAX_127) {
			return 127;
		} else if (intResult < MIDI_MIN_0) {
			return MIDI_MIN_0;
		} else {
			return intResult;
		}
	}

	public static int getY(int x) {
		if (results == null) {
			return 0;
		}
		if (results.length == 0) {
			return 0;
		}
		if (x >= results.length) {
			return 0;
		}
		try {
			return results[x];
		} catch (Exception e) {
			LOG.error("x: {}", x, e);
			return 0;
		}
	}

	public static Map<String, Function> getFormulaMap() {
		List<Function> listOfFunctions = JsonReader.readFunctionsFromFile();

		if (listOfFunctions == null || listOfFunctions.isEmpty()) {
			return defaultFormulaMap;
		}
		Map<String, Function> mapOfFunctions = listOfFunctions.stream()
				.collect(Collectors.toMap(Function::getName, function -> function));

		return mapOfFunctions;
	}

	public static void main(String[] args) {
		System.out.println(getFormulaMap());
	}
}
