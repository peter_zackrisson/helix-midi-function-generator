package zack.helixmidifunctiongenerator;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Point2D;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class DataPanel implements ActionListener, ItemListener {
	private static Logger LOG = LoggerFactory.getLogger(DataPanel.class);

	public static final String DEFAULT_FUNCTION_SINE = "Sine";
	private boolean initialized;
	private final JFrame myFrame;
	private JPanel myPanel;
	private String myTitle;
	private String myXTitle;
	private String myYTitle;
	private float xLower, xUpper, xInterval;
	private float yLower, yUpper, yInterval;
	private Point2D.Float[] myPoints;
	private JTextArea myFormulaField;
	private MidiSender myMidiSender;
	private JTextField myMidiChannel;
	private JTextField myMidiCC;
	private JTextArea myStatusValue;
	private GraphPanel myGraphPanel;
	private JTextField myNrOfValues;
	private JTextField mySleepTime;
	private Map<String, Function> myFormulaMap;
	private JComboBox<String> myFormulaList;
	private final Instance myInstance;

	DataPanel(JFrame newFrame, Instance instance) {
		myInstance = instance;
		initialized = false;
		myFrame = newFrame;
		myPanel = new JPanel(new FlowLayout());
		myFrame.getContentPane().add(myPanel, "East");
		actionPerformed(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		LOG.debug("actionPerformed DataPanel {}", e);
		initialized = initData();
		myPanel.update(myPanel.getGraphics());
		myFrame.pack();
		myFrame.setVisible(true);
	}

	private boolean initData() {
		LOG.debug("initData");
		myFrame.getContentPane().remove(myPanel);
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();

		myPanel = new JPanel(gridBag);

		// Formula
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0.7;
		c.weighty = 4.0;
		JLabel formulaLabel = new JLabel("Formula");
		gridBag.setConstraints(formulaLabel, c);
		myPanel.add(formulaLabel);

		Function function = FunctionGenerator.getFormulaMap().get(myInstance.getFunction());
		if (function == null) {
			function = FunctionGenerator.getFormulaMap().get(DEFAULT_FUNCTION_SINE);
		}

		String functionStr = function.getFunction();

		myFormulaField = new JTextArea(functionStr);
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(myFormulaField, c);
		myPanel.add(myFormulaField);

		// Formulas dropdown
		myFormulaMap = FunctionGenerator.getFormulaMap();
		String[] formulaKeys = myFormulaMap.keySet().toArray(new String[myFormulaMap.size()]);
		SortedComboBoxModel<String> model = new SortedComboBoxModel<String>(formulaKeys);
		myFormulaList = new JComboBox<String>(model);
		myFormulaList.setEditable(true);

		c.weighty = 1.0;
		JLabel formulasLabel = new JLabel("Formulas");
		c.weightx = 0.7;
		c.gridwidth = GridBagConstraints.LINE_START;
		gridBag.setConstraints(formulasLabel, c);
		myPanel.add(formulasLabel);

		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(myFormulaList, c);
		myPanel.add(myFormulaList);

		// Midi channel
		c.weighty = 1.0;
		JLabel midiChannelLabel = new JLabel("Midi channel");
		c.weightx = 0.7;
		c.gridwidth = GridBagConstraints.LINE_START;
		gridBag.setConstraints(midiChannelLabel, c);
		myPanel.add(midiChannelLabel);

		myMidiChannel = new JTextField(String.valueOf(myInstance.getMidiChannel()));
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(myMidiChannel, c);
		myPanel.add(myMidiChannel);

		// CC
		c.weightx = 0.7;
		c.gridwidth = GridBagConstraints.LINE_START;
		JLabel midiCCLabel = new JLabel("CC");
		gridBag.setConstraints(midiCCLabel, c);
		myPanel.add(midiCCLabel);

		myMidiCC = new JTextField(String.valueOf(myInstance.getMidiCC()));
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(myMidiCC, c);
		myPanel.add(myMidiCC);

		// # Values
		c.weightx = 0.7;
		c.gridwidth = GridBagConstraints.LINE_START;
		JLabel nrOfValuesLabel = new JLabel("# Values");
		gridBag.setConstraints(nrOfValuesLabel, c);
		myPanel.add(nrOfValuesLabel);

		myNrOfValues = new JTextField(String.valueOf(FunctionGenerator.NUMBER_OF_VALUES));
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(myNrOfValues, c);
		myPanel.add(myNrOfValues);

		// Sleep
		c.weightx = 0.7;
		c.gridwidth = GridBagConstraints.LINE_START;
		JLabel sleepTimeLabel = new JLabel("Sleep time [ms]");
		gridBag.setConstraints(sleepTimeLabel, c);
		myPanel.add(sleepTimeLabel);

		mySleepTime = new JTextField(MidiSender.DEFAULT_SLEEP_TIME_IN_MS);
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(mySleepTime, c);
		myPanel.add(mySleepTime);

		// Status
		c.weightx = 0.7;
		c.gridwidth = GridBagConstraints.LINE_START;
		JLabel statusLabel = new JLabel("Status");
		gridBag.setConstraints(statusLabel, c);
		myPanel.add(statusLabel);

		c.gridwidth = GridBagConstraints.REMAINDER;
		myStatusValue = new JTextArea("");
		gridBag.setConstraints(myStatusValue, c);
		myPanel.add(myStatusValue);

		myFrame.getContentPane().add(myPanel, "East");

		// Put this last in order not to trigger action to be called before all gui
		// objects are initialised
		myFormulaList.setActionCommand("ComboBoxChanged");
		myFormulaList.addItemListener(this);
		myFormulaList.setSelectedItem(myInstance.getFunction()); // DEFAULT_FUNCTION_SINE

		return true;
	}

	// Read data from panel in case user made any changes
	void refreshData() {
		LOG.info("refreshData");
		StringBuilder sb = new StringBuilder();

		if (myMidiSender != null) {
			sb.append("Connection: " + myMidiSender.getConnectionStatus());
			sb.append(System.getProperty("line.separator"));
			sb.append(myMidiSender.getSenderStatus());
		}
		myStatusValue.setText(sb.toString());
		return;
	}

	public JFrame getFrame() {
		return myFrame;
	}

	boolean isInitialized() {
		return initialized;
	}

	String getTitle() {
		return myTitle == null ? "-" : myTitle;
	}

	String getXTitle() {
		return myXTitle == null ? "-" : myXTitle;
	}

	String getYTitle() {
		return myYTitle == null ? "-" : myYTitle;
	}

	float getXLower() {
		return xLower;
	}

	float getXUpper() {
		return xUpper;
	}

	float getXInterval() {
		return xInterval;
	}

	float getYLower() {
		return yLower;
	}

	float getYUpper() {
		return yUpper;
	}

	float getYInterval() {
		return yInterval;
	}

	int getNumberOfPoints() {
		return myPoints.length;
	}

	Point2D.Float getPoint(int i) {
		if (i < 0) {
			i = 0;
		} else if (i >= myPoints.length) {
			i = myPoints.length - 1;
		}
		return myPoints[i];
	}

	public String getFormula() {
		return myFormulaField.getText();
	}

	public int getMidiCC() {
		try {
			return Integer.valueOf(myMidiCC.getText()).intValue();
		} catch (NumberFormatException e) {
			LOG.error("Error parsing Midi CC. Using default: {}.", MidiSender.DEFAULT_MIDI_CC);
			return MidiSender.DEFAULT_MIDI_CC;
		}
	}

	public int getNrOFvalues() {
		try {
			return Integer.valueOf(myNrOfValues.getText()).intValue();
		} catch (NumberFormatException e) {
			LOG.error("Error parsing number of values. Using default: {}.", FunctionGenerator.NUMBER_OF_VALUES);
			return FunctionGenerator.NUMBER_OF_VALUES;
		}
	}

	public int getSleepTime() {
		try {
			return Integer.valueOf(mySleepTime.getText()).intValue();
		} catch (NumberFormatException e) {
			LOG.error("Error parsing sleep time. Using default: {} [ms].", MidiSender.DEFAULT_SLEEP_TIME_IN_MS);
			return MidiSender.DEFAULT_SLEEP_TIME_IN_MS;
		}
	}

	public int getMidiChannel() {
		try {
			return Integer.valueOf(myMidiChannel.getText()).intValue();
		} catch (NumberFormatException e) {
			LOG.error("Error parsing Midi Channel. Using default: {} [ms].", MidiSender.DEFAULT_MIDI_CHANNEL);
			return MidiSender.DEFAULT_MIDI_CHANNEL;
		}
	}

	public void setMidiSender(MidiSender midisender) {
		myMidiSender = midisender;
		myMidiCC.setText(String.valueOf(midisender.getCC()));
		myMidiChannel.setText(String.valueOf(midisender.getChannel()));
		myMidiSender.setDataPanel(this);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		LOG.debug("itemStateChanged", e);
		if (e.getStateChange() == ItemEvent.SELECTED) {
			@SuppressWarnings("unchecked")
			JComboBox<String> cb = (JComboBox<String>) e.getSource();
			String formulaKey = (String) cb.getSelectedItem();
			LOG.debug("formulaKey: {}", formulaKey);
			updateFields(formulaKey);
		}
		if (myGraphPanel != null) {
			myGraphPanel.actionPerformed(null); // Plot
		}
	}

	private void updateFields(String formulaKey) {
		Function function = FunctionGenerator.getFormulaMap().get(formulaKey);
		if (function != null) {
			myFormulaField.setText(function.getFunction());
			mySleepTime.setText(String.valueOf(function.getSleepTime()));
			myNrOfValues.setText(String.valueOf(function.getValues()));
		} else {
			LOG.error("Function with key {} not found.", formulaKey);
		}
	}

	public void setGraphPanel(GraphPanel graphpanel) {
		myGraphPanel = graphpanel;
	}

	public String getFormulaName() {
		if (myFormulaList.getItemCount() >= 0) {
			Object selectedItem = myFormulaList.getSelectedItem();
			if (selectedItem != null) {
				return selectedItem.toString();
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	public Map<String, Function> getFunctionMap() {
		return myFormulaMap;
	}

	public void resetComboBox(String formulaToBeDeleted) {
		int currentIndex = myFormulaList.getSelectedIndex();
		if (currentIndex > -1) {
			myFormulaList.removeItemListener(this);
			myFormulaList.removeItem(formulaToBeDeleted);
			// myFormulaList.getSelectedIndex();
			if (myFormulaList.getItemCount() > 0) {
				myFormulaList.setSelectedIndex(0);
				String formulaKey = (String) myFormulaList.getSelectedItem();
				updateFields(formulaKey);
				if (myGraphPanel != null) {
					myGraphPanel.actionPerformed(null); // Plot
				}
			} else {
				myFormulaField.setText("");
			}
			myFormulaList.addItemListener(this);
		}
	}
}