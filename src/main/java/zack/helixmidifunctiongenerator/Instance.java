package zack.helixmidifunctiongenerator;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class Instance {

	@JsonProperty("InstanceName")
	private String InstanceName;

	@JsonProperty("Function")
	private String Function;

	@JsonProperty("MidiChannel")
	private Integer MidiChannel;

	@JsonProperty("MidiCC")
	private Integer MidiCC;

	@JsonProperty("Start")
	private Boolean Start = false;

	public Boolean getStart() {
		return Start;
	}

	public void setStart(Boolean start) {
		Start = start;
	}

	private MidiSender midiSender;

	public MidiSender getMidiSender() {
		return midiSender;
	}

	public void setMidiSender(MidiSender midiSender) {
		this.midiSender = midiSender;
	}

	// Without this default constructor json reader will not work
	public Instance() {
		super();
	}

	public Instance(String instanceName, String function, int midiChannel, int midiCC) {
		this.InstanceName = instanceName;
		this.Function = function;
		this.MidiChannel = midiChannel;
		this.MidiCC = midiCC;
	}

	public String getFunction() {
		return Function;
	}

	public void setFunction(String function) {
		Function = function;
	}

	@Override
	public String toString() {
		return "Instance [InstanceName=\"" + InstanceName + "\", Function=\"" + Function + "\", MidiChannel="
				+ MidiChannel + ", MidiCC=" + MidiCC + "]";
	}

	public String getInstanceName() {
		return InstanceName;
	}

	public void setInstanceName(String instanceName) {
		InstanceName = instanceName;
	}

	public Integer getMidiChannel() {
		return MidiChannel;
	}

	public void setMidiChannel(Integer midiChannel) {
		MidiChannel = midiChannel;
	}

	public Integer getMidiCC() {
		return MidiCC;
	}

	public void setMidiCC(Integer midiCC) {
		MidiCC = midiCC;
	}
}
