package zack.helixmidifunctiongenerator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonWriter implements ActionListener {
	private static Logger LOG = LoggerFactory.getLogger(JsonWriter.class);

	private static final String FUNCTION_FILENAME = "./functions.json";
	private DataPanel myDataPanel;

	public JsonWriter(DataPanel dataPanel) {
		myDataPanel = dataPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) { // Action called when Save
		LOG.debug("actionPerformed {}", e);
		if (e == null) {
			return;
		}
		switch (e.getActionCommand()) {
		case HelixMidiFunctionGenerator.SAVE_COMMAND:
			if (!myDataPanel.getFormulaName().isEmpty()) {
				Function function = new Function(myDataPanel.getFormulaName(), myDataPanel.getFormula(),
						myDataPanel.getNrOFvalues(), myDataPanel.getSleepTime());
				Map<String, Function> functionMap = myDataPanel.getFunctionMap();
				functionMap.put(myDataPanel.getFormulaName(), function);
				writeFunctions(functionMap);

			} else {
				LOG.warn("Formula name is empty. Will not save.");
			}
			break;

		case HelixMidiFunctionGenerator.DELETE_COMMAND:
			Map<String, Function> functionMap = myDataPanel.getFunctionMap();
			String formulaToBeDeleted = myDataPanel.getFormulaName();
			if (formulaToBeDeleted.length() > 0) {
				functionMap.remove(formulaToBeDeleted);
				writeFunctions(functionMap);

				myDataPanel.resetComboBox(formulaToBeDeleted);
				LOG.info("Deleted function: {}", formulaToBeDeleted);
				LOG.debug("FunctionMap: {}", myDataPanel.getFunctionMap());
			}
			break;
		}
	}

	private void writeFunctions(Map<String, Function> functionMap) {
		ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
		try {
			mapper.writeValue(new File(FUNCTION_FILENAME), functionMap.values());
		} catch (IOException e1) {
			LOG.error("Error writing functions", e1);
		}
	}
}
