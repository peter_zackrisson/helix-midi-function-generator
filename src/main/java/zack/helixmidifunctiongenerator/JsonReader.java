package zack.helixmidifunctiongenerator;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonReader {
	private static final String FUNCTION_FILENAME = "./functions.json";

	public static void main(String[] args) {
		System.out.println(readFunctionsFromFile());
	}

	public static List<Function> readFunctionsFromFile() {
		ObjectMapper mapper = new ObjectMapper();

		try {
			List<Function> obj = mapper.readValue(new File(FUNCTION_FILENAME), new TypeReference<List<Function>>() {
			});
			return obj;
		} catch (IOException e) {
			return null;
		}
	}
}
