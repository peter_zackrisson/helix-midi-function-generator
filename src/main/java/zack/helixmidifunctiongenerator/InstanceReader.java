package zack.helixmidifunctiongenerator;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class InstanceReader {
	private static Logger LOG = LoggerFactory.getLogger(InstanceReader.class);
	public static final String INSTANCES_FILENAME = "./instances.json";

	public static void main(String[] args) {
		System.out.println(readFunctionsFromFile());
	}

	public static List<Instance> readFunctionsFromFile() {
		ObjectMapper mapper = new ObjectMapper();

		try {
			List<Instance> obj = mapper.readValue(new File(INSTANCES_FILENAME), new TypeReference<List<Instance>>() {
			});
			return obj;
		} catch (IOException e) {
			LOG.error("Error reading instance", e);
			return null;
		}
	}
}
