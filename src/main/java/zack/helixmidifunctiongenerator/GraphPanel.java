package zack.helixmidifunctiongenerator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class GraphPanel implements ActionListener {
	private static Logger LOG = LoggerFactory.getLogger(GraphPanel.class);
	private final DataPanel datapanel;
	private final JFrame frame;
	private final GraphicPanel panel;
	private final Instance myInstance;

	GraphPanel(JFrame newFrame, Instance instance) {
		myInstance = instance;
		frame = newFrame;
		panel = new GraphicPanel();
		panel.setDisplayPlot(false);
		datapanel = new DataPanel(frame, instance);
		panel.setDataPanel(datapanel);
		frame.getContentPane().add(panel, "Center");
		actionPerformed(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!datapanel.isInitialized()) {
			LOG.warn("actionPerformed GraphPanel datapanel not initialized", e);
			return;
		}
		datapanel.refreshData();
		myInstance.getMidiSender().setCC(datapanel.getMidiCC());
		myInstance.getMidiSender().setChannel(datapanel.getMidiChannel());
		LOG.debug("actionPerformed GraphPanel {}", e);
		panel.setDisplayPlot(true);
		panel.update(panel.getGraphics());
		frame.setSize(700, 600);
		frame.setVisible(true);
		frame.pack();
	}

	DataPanel getDataPanel() {
		return datapanel;
	}
}
