package zack.helixmidifunctiongenerator;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.MidiSystem;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class MidiDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = -4824306879097421665L;
	private final JPanel contentPanel = new JPanel();
	private Item myCurrentItem;
	private MidiSender myMidiSender;
	DataPanel myDataPanel;
	private static Logger LOG = LoggerFactory.getLogger(MidiDialog.class);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MidiDialog dialog = new MidiDialog(new MidiSender(), null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 * 
	 * @param myDataPanel
	 */
	public MidiDialog(MidiSender midiSender, DataPanel dataPanel) {
		myDataPanel = dataPanel;
		myMidiSender = midiSender;
		setBounds(100, 100, 600, 150);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(
				new FormLayout(new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
						new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
								FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, }));
		{
			Info[] devices = MidiSystem.getMidiDeviceInfo();
			List<Item> items = Arrays.stream(devices)
					.map(b -> new Item(b, b.getName(), b.getVendor(), b.getVersion(), b.getDescription()))
					.collect(Collectors.toList());

			{
				JLabel lblNewLabel = new JLabel("Select midi interface");
				contentPanel.add(lblNewLabel, "2, 2");
			}
			// JComboBox comboBox = new JComboBox(a);
			myCurrentItem = items.get(0);
			JComboBox<Object> comboBox = new JComboBox<Object>(items.toArray());
			comboBox.addActionListener(this);
			contentPanel.add(comboBox, "2, 4, fill, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(e -> okClick(e));
				getRootPane().setDefaultButton(okButton);
				buttonPane.add(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.addActionListener(e -> cancelClick(e));
				buttonPane.add(cancelButton);
			}
		}
	}

	private void okClick(ActionEvent e) {
		LOG.info("okClick: " + myCurrentItem);
		myMidiSender.setHelixMidiTransmitter(myCurrentItem.getDevice());
		myDataPanel.refreshData();
		dispose();
	}

	private void cancelClick(ActionEvent e) {
		LOG.info("cancelClick");
		dispose();
	}

	class Item {

		private String name;
		private String vendor;
		private String version;
		private String description;
		private Info device;

		public Item(Info device, String name, String vendor, String version, String description) {
			this.device = device;
			this.name = name;
			this.vendor = vendor;
			this.version = version;
			this.description = description;
		}

		public String getName() {
			return name;
		}

		public Info getDevice() {
			return device;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name + " - " + vendor + " - " + version + " - " + description;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JComboBox) {
			@SuppressWarnings("unchecked")
			JComboBox<Item> combo = (JComboBox<Item>) e.getSource();
			Item item = (Item) combo.getSelectedItem();
			myCurrentItem = item;
		}
	}
}
