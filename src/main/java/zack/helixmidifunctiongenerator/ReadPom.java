package zack.helixmidifunctiongenerator;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.Manifest;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadPom {

	private static Logger LOG = LoggerFactory.getLogger(ReadPom.class);

	public static void main(String[] args) throws IOException, XmlPullParserException {
		MavenXpp3Reader reader = new MavenXpp3Reader();
		Model model = reader.read(new FileReader("pom.xml"));
		System.out.println(model.getId());
		System.out.println(model.getGroupId());
		System.out.println(model.getArtifactId());
		System.out.println(model.getVersion());
	}

	public String getVersion() {

		String v = getVersionFromManifest();
		if (v != null) {
			return v;
		}

		MavenXpp3Reader reader = new MavenXpp3Reader();
		Model model = null;
		try {
			model = reader.read(new FileReader("pom.xml"));
			return model.getVersion();
		} catch (IOException | XmlPullParserException e) {
			LOG.error("Error parsing pom.xml", e);
		}
		return "";
	}

	public String getVersionFromManifest() {
		Enumeration<URL> resources;
		try {
			resources = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
			while (resources.hasMoreElements()) {
				try {
					Manifest manifest = new Manifest(resources.nextElement().openStream());
					String av = manifest.getMainAttributes().getValue("appversion");
					return av;
				} catch (IOException e2) {
					LOG.error("Error getting manifest appversion", e2);
				}
			}
		} catch (IOException e) {
			LOG.error("Error reading manifest", e);
		}
		return null;
	}
}
