## Helix Midi Function Generator

This project creates a java program to generate MIDI control signal towards Line6 Helix based on an arbitrary function
and can control any Helix parameter.

Multiple instances can be run in parallel.

### Download latest

[helixmidifunctiongenerator.jar](https://bitbucket.org/peter_zackrisson/helix-midi-function-generator/downloads/helixmidifunctiongenerator.jar)

### How to build
```
mvn clean install
```

Generates a jar file in target directory

### Prerequisites

* java >=8
* maven


### How to run

Double-click on generated jar or run with

```
java -jar target/helixmidifunctiongenerator.jar
```

Make sure the Helix channel in GUI corresponds to your settings in Helix

In HX Edit (or directly on Helix) connect a parameter to MIDI CC (Continuous Controller) 

### Note about functions

The generator goes from 0 to 99 (or configured value). For each value (x) the function on top right is called generating a value (y) send as MIDI CC value in range of 0 to 127.

These values will be sent with a sleep of 10ms (or configured value) between each values. Decreasing this tends to make Helix more unstable.

"Any" javascript function should be possible to use, but character x will be replaced by values 0 to 99.
This means Math.cosx will not work at the moment. This is on my to do list.

Instead of Math.PI it is possible to simply use π

You can select between some pre-generated functions.

Functions are stored in a file called *functions.json* in the directory used for starting the application.

## Starting multiple instances

If application started with -i

```
java -jar target/helixmidifunctiongenerator.jar -i
```

It will read an instances.json file in which it is possible to set InstanceName, Function, MidiChannel, MidiCC and Start
enabling starting multiple instances in one go and immediately start sending.

## Examples

![Screenshot](src/site/resources/HelixMidiFunctionGenerator.png)

[Sound example](https://bitbucket.org/peter_zackrisson/helix-midi-function-generator/src/master/src/site/resources/HelixMidiFunctionGenerator.mp3)

[Helix patch](https://bitbucket.org/peter_zackrisson/helix-midi-function-generator/src/master/src/site/resources/HelixMidiFunctionGenerator.hlx)

## TODO

* Remove need to write Math in function
* Improve GUI. Does not scale very well.

## Credits

The graphics part of this project was inspired by [https://kodejava.org/how-do-i-draw-plot-a-graph/](https://kodejava.org/how-do-i-draw-plot-a-graph/)

Functions are inspired by [https://stackoverflow.com/questions/1073606/is-there-a-one-line-function-that-generates-a-triangle-wave](https://stackoverflow.com/questions/1073606/is-there-a-one-line-function-that-generates-a-triangle-wave)

## Contact

If you find this program useful, if you want to suggest improvement or add new cool functions, please send an email to

![mail](src/site/resources/Mail.png)
